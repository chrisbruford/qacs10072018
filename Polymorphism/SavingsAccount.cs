﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Polymorphism
{
    class SavingsAccount : Account
    {
        public int Term { get; } = 24;

        public SavingsAccount(string sortCode, decimal openingBalance, string customer) 
            : base(openingBalance,customer) {

        }

        public override decimal Withdraw(decimal amount)
        {
            Console.WriteLine("After 100 days your money will be available to you");
            return base.Withdraw(amount);
        }

        public override decimal Deposit(decimal amount)
        {
            return 0;
        }
    }
}
