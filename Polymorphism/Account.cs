﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Polymorphism
{
    abstract class Account
    {
        public string Customer { get; set; }
        public string SortCode { get; }
        public string AccountNumber { get; }
        public decimal Balance { get; private set; } = 0;

        public Account(decimal balance, string customer) {
            Balance = balance;
            Customer = customer;
        }

        public virtual decimal Withdraw(decimal amount)
        {
            Balance -= amount;
            Console.WriteLine($"You have withdrawn {amount} funds");
            return Balance;
        }

        public abstract decimal Deposit(decimal amount);
        //{
        //    Balance += amount;
        //    Console.WriteLine($"You have deposited {amount} funds");
        //    return Balance;
        //}
    }
}
