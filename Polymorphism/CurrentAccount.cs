﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Polymorphism
{
    class CurrentAccount : Account
    {

        public decimal Overdraft { get; } = 0;

        public CurrentAccount(decimal overdraft, decimal openingBalance, string customer)
            : base(openingBalance, customer)
        {
            Overdraft = overdraft;
        }

        public override decimal Deposit(decimal amount)
        {
            return 0;
        }
    }
}
