﻿using System;
using System.Collections.Generic;

namespace Polymorphism
{
    class Program
    {
        static void Main(string[] args)
        {
            var currentAccount = new CurrentAccount(0,0,"Matt");
            var savingsAccount = new SavingsAccount("23-54-34",0,"Ting");

            //cannot instantiate abstract classes
            //var account = new Account(500, "Chris");

            var accounts = new List<Account>() { currentAccount, savingsAccount };

            SavingsAccount theCurrentAccount = accounts[0] as SavingsAccount;
            Console.WriteLine(accounts[0].Balance);

            savingsAccount.Deposit(100);
            currentAccount.Deposit(200);
            savingsAccount.Withdraw(50);
            currentAccount.Withdraw(25);

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
