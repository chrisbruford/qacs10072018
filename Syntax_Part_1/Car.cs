﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Syntax_Part_1
{
    class Car
    {
        private float speed;
        float newSpeed;

        public void Accelerate(float speed)
        {
            newSpeed = this.speed + speed;
            this.speed = newSpeed;
        }

        public void Brake(float speed)
        {
            Console.WriteLine(newSpeed);
            this.speed -= speed;
        }

        public static void Park(Car car)
        {
            car.Brake(0);
            Console.WriteLine("I parked your car for you sir/madam!");
        }

        public static void SayHello(string name)
        {
            Console.WriteLine($"Hello {name}");
        }
    }
}
