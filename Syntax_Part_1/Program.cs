﻿using System;

namespace Syntax_Part_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //This is a single-line comment

            /*this
             * is 
             * a 
             * multiline
             * comment
            */

            //int number = 6;
            //byte myByte = 255;

            //bool myBool = true;
            //string myString = "This is a string literal";
            //char myChar = 'c';



            Car myCar = new Car();
            SuperCar mySuperCar = new SuperCar();

            RaceCars(myCar, mySuperCar);

            //myCar.Accelerate(50);
            //Car.Park(myCar);
            //Car.SayHello("Ula");

            //mySuperCar.Accelerate(200);

            RaceCars(myCar, mySuperCar);
            RaceCars(car2: mySuperCar, car1: myCar, breakTime: false);
            //mySuperCar.engageTurbo();

            //just to prove that chars can be converted to string
            //char b = 'b';
            //b.ToString();
            //stringThings(b);

            //Console.WriteLine(mySuperCar.ToString());

            //QAMaths();
            //Loops();
            Switches(543);
            Console.ReadKey();
        }

        public static void RaceCars(Car car1, Car car2, bool breakTime = true)
        {
            Console.WriteLine(breakTime);
            //if (car2 is SuperCar)
            if (car2.GetType() == typeof(SuperCar))
            {
                Console.WriteLine(car2.GetType());
                ((SuperCar)car2).engageTurbo();
                Console.WriteLine("Car 2 won by a landslide");
                return;
            }

            Console.WriteLine("Car2 won by a nose");
        }

        public static void RaceCars(Car car1, SuperCar car2, bool breakTime = true)
        {
            car2.engageTurbo();
            Console.WriteLine("Car 2 wins because it's a superCar");
        }

        public static void stringThings(char x)
        {
   
            
        }

        public static void checkFive()
        {
            //cast car2 into a SuperCar
            if (5 > 5)
            {
                Console.WriteLine("If succeeded");
            }
            else if (5 < 5)
            {
                Console.WriteLine("Else if succeeded");
            }
            else if (false)
            {
                //this is never true so never runs
            }
            else
            {
                Console.WriteLine("If failed....");
            }
        }

        public static void QAMaths()
        {
            Console.WriteLine(3F / 2);
        }

        public static void Loops()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5) { continue; }
                Console.WriteLine(i);
            }
        }

        public static void Switches(int number)
        {
            switch (number)
            {
                case 0:
                    Console.WriteLine("No");
                    break;
                case 1:
                    Console.WriteLine("Just one");
                    break;
                case 2:
                    Console.WriteLine("A couple");
                    break;
                default:
                    Console.WriteLine("A few");
                    break;
            }
        }
    }
}
