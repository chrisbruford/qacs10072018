﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Generics
{
    class Bank
    {
        //Account[] accounts = new Account[5];
        //ArrayList accounts = new ArrayList();
        List<Account> accounts = new List<Account>();
        List<Customer> customers = new List<Customer>();
        Dictionary<string,Branch> branches = new Dictionary<string,Branch>();

        public Bank()
        {
            Branch branch = new Branch() { address = "The High Street", sortCode = "40-38-29" };
            //branch.address = "The High Street";
            //branch.sortCode = "40-38-29";

            branches.Add("London", branch);
            branches.Add("Manchester", new Branch());
            branches.Add("Glasgow", new Branch());
            branches.Add("Leeds", new Branch());
        }

        public List<Account> Accounts
        {
            get
            {
                return accounts;
            }
        }

        public List<Customer> Customers
        {
            get
            {
                return customers;
            }
        }

        public Dictionary<string, Branch> Branches
        {
            get
            {
                return branches;
            }
        }

        public void addAccount(Account account)
        {
            accounts.Add(account);
        }

        public void addCustomer(Customer customer)
        {
            customers.Add(customer);
        }
    }
}
