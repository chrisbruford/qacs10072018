﻿using System;

namespace Generics
{
    class Program
    {
        static void Main(string[] args)
        {
            Bank bank = new Bank();

            Account micheleAccount = new Account("michele");
            Account ulaAccount = new Account("ula");
            Account mattAccount = new Account("matt");

            bank.addAccount(micheleAccount);
            bank.addAccount(ulaAccount);
            bank.addAccount(mattAccount);

            bank.addCustomer(new Customer());

            foreach (Account account in bank.Accounts)
            {
                Console.WriteLine($"Account holder: {account.customer}");
            }

            foreach (Customer customer in bank.Customers)
            {
                Console.WriteLine(customer);
            }

            Console.WriteLine(bank.Branches["Leeds"]);

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
