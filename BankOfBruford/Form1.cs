﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BankLibrary;

namespace BankOfBruford
{
    public partial class Form1 : Form
    {
        private Bank bank;

        public Form1()
        {
            bank = new Bank();
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Customer newCustomer = new Customer();
            newCustomer.Firstname = textBox1.Text;
            newCustomer.Surname = textBox2.Text;

            bank.Customers.Add(newCustomer);

            Account newAccount = new Account();

            newCustomer.Postcode = "SM7 1BN";

            MessageBox.Show($"{newCustomer.Firstname} {newCustomer.Surname} of {newCustomer.Postcode} has been added to the bank");
        }
    }
}
