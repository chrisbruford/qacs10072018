﻿using System;
using System.Collections.Generic;

namespace Delegates
{
    class Program
    {
        static void Main(string[] args)
        {
            List<CurrentAccount> accounts = new List<CurrentAccount>() {
                new CurrentAccount() { AccountHolder="Alistair", AccountNumber="123456789", Balance=1000000 },
                new CurrentAccount() { AccountHolder="Michele", AccountNumber="123456789", Balance=2000000 },
                new CurrentAccount() { AccountHolder="Ula", AccountNumber="123456789", Balance=3000000 },
                new CurrentAccount() { AccountHolder="Zaid", AccountNumber="123456789", Balance=4000000 },
                new CurrentAccount() { AccountHolder="George", AccountNumber="123456789", Balance=10000000 },
                new CurrentAccount() { AccountHolder="Cale", AccountNumber="123456789", Balance=10000010 }
            };

            List<SavingsAccount> savingsAccounts = new List<SavingsAccount>() {
                new SavingsAccount() { AccountHolder="Alistair", AccountNumber="123456789", Balance=1000000 },
                new SavingsAccount() { AccountHolder="Michele", AccountNumber="123456789", Balance=2000000 },
                new SavingsAccount() { AccountHolder="Ula", AccountNumber="123456789", Balance=3000000 },
                new SavingsAccount() { AccountHolder="Zaid", AccountNumber="123456789", Balance=4000000 },
                new SavingsAccount() { AccountHolder="George", AccountNumber="123456789", Balance=10000000 },
                new SavingsAccount() { AccountHolder="Cale", AccountNumber="123456789", Balance=10000010 }
            };

            //bool FilterByValue(CurrentAccount currentAccount)
            //{
            //    return currentAccount.Balance > 5000000;
            //}

            bool FilterByName(CurrentAccount currentAccount)
            {
                return currentAccount.AccountHolder.Contains("Cale");
            }

            bool FilterByNonsense(CurrentAccount currentAccount)
            {
                Console.WriteLine("This is totally unnecessary");
                Console.WriteLine("No honestly, why would you do this?");
                return currentAccount.AccountNumber.Contains("1");
            }

            //FilterDelegate filterDelegate = new FilterDelegate(currentAccount => currentAccount.Balance > 5000000);
            //List<CurrentAccount> filteredAccounts = Bank.Where(accounts, currentAccount => currentAccount.Balance > 5000000);
            //foreach (CurrentAccount account in filteredAccounts)
            //{
            //    Console.WriteLine($"{account.AccountHolder} has £{account.Balance}");
            //}

            //List<CurrentAccount> filteredAccounts2 = Bank.Where(accounts, filterDelegate2);
            //FilterDelegate filterDelegate2 = new FilterDelegate(FilterByName);
            //foreach (CurrentAccount account in filteredAccounts2)
            //{
            //    Console.WriteLine($"{account.AccountHolder} has £{account.Balance}");
            //}

            //delegates are useful to make things look tidy and reusable
            //var filteredAccounts3 = Bank.Where(accounts, new FilterDelegate(FilterByNonsense));
            //var filteredAccounts3 = Bank.Where(accounts, FilterByNonsense);

            var filteredAccounts4 = Bank.Where(accounts, currentAccount => currentAccount.AccountHolder.Contains("Alistair"));
            foreach (CurrentAccount account in filteredAccounts4)
            {
                Console.WriteLine($"{account.AccountHolder} has £{account.Balance}");
            }

            var filteredAccounts5 = Bank.Where(savingsAccounts, savingsAccount => savingsAccount.AccountHolder.Contains("Alistair"));
            foreach (SavingsAccount account in filteredAccounts5)
            {
                Console.WriteLine($"{account.AccountHolder} has £{account.Balance}");
            }

            Console.WriteLine("press any key to exit...");
            Console.ReadKey();
        }

        
    }
}
