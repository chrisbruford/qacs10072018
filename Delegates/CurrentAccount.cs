﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegates
{
    class CurrentAccount
    {
        public string AccountNumber { get; set; }
        public string AccountHolder { get; set; }
        public decimal Balance { get; set; }
    }
}
