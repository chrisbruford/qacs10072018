﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Delegates
{
    class Bank
    {
        public static List<T> Where<T>(List<T> currentAccounts, Predicate<T> filterDelegate)
        {
            var filteredList = new List<T>();

            //filter by people who have a certain amount of money
            foreach (T account in currentAccounts)
            {
                //rich people list
                //if (account.Balance > amount)
                //{
                //    filteredList.Add(account);
                //}

                //using delegate
                if (filterDelegate(account))
                {
                    filteredList.Add(account);
                }
            }


            return filteredList;
        }
    }

    delegate bool FilterDelegate<T>(T thing);
}
