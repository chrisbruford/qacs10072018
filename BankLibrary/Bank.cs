﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLibrary
{
    public class Bank
    {
        public List<Customer> Customers { get; } = new List<Customer>();
        public List<Account> Accounts { get; } = new List<Account>();
    }
}
