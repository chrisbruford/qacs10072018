﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BankLibrary
{
    public class Customer
    {
        private string postcode;

        public string Postcode
        {
            get {
                return postcode;
            }

            set {
                postcode = value;
            }
        }

        public string Firstname { get; set; }
        public string Surname { get; set; }
    }
}
