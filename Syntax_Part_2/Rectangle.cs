﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Syntax_Part_2
{
    struct Rectangle
    {
        float shortLength;
        float longLength;
        Colour colour;

        //constants cannot be changed
        public const int maxLength = 100;

        //readonly must be initialised at the time or during construction
        public readonly DateTime createdAt;

        public Rectangle(float shortLength, float longLength, Colour colour)
        {
            this.shortLength = shortLength;
            this.longLength = longLength;
            this.colour = colour;
            createdAt = new DateTime();
        }
    }
}
