﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Syntax_Part_2
{
    static class Utils
    {
        public static int WordCount(this string theString)
        {
            string[] words = theString.Split(" ");
            return words.Length;
        }

        public static int WordCount(this string theString, int max)
        {
            string[] words = theString.Split(" ");

            //if (words.Length > max)
            //{
            //    return max;
            //} else
            //{
            //    return words.Length;
            //}

            return words.Length > max ?  max : words.Length;
        }
    }
}
