﻿using System;

namespace Syntax_Part_2
{
    class Program
    {
        static void Main(string[] args)
        {
            {
                //value types
                //int myNumber = 10;
                //Console.WriteLine(myNumber);
                //myNumber = ChangeValue(myNumber);
                //Console.WriteLine(myNumber);

                ////ref
                //int myNumber = 10;
                //Console.WriteLine(myNumber);
                //ChangeValue(ref myNumber);
                //Console.WriteLine(myNumber);

                //out
                //int newValue;
                //CreateValue(out newValue);
                //Console.WriteLine(newValue);
            }

            {
                //reference types
                //Car car = new Car();
                //car.Wheels = 4;
                //Console.WriteLine(car.Wheels);
                //ChangeValue(car);
                //Console.WriteLine(car.Wheels);

                //string myString = "original string";
                //Console.WriteLine(myString);
                //ChangeValue(myString);
                //Console.WriteLine(myString);

                //Ref types are nullable by default
                //Value types can be made nullable with the ? operator
                //Car someCar = null;
                //int? someNumber = null;

                //structs are value-types that are used primarily for their data
                //Rectangle rectangle = new Rectangle(10,20, Colour.Hotpink);
                //Console.WriteLine(rectangle.createdAt);

                //Ternary if
                //int age = 18;

                //if (age >= 18)
                //{
                //    Console.WriteLine("Served!");
                //} else
                //{
                //    Console.WriteLine("Do you have any ID?");
                //}

                //this is the same as the above if-statement (just shorter!)
                //Console.WriteLine(age >= 18 ? "Served!" : "Do you have ID?");

                //int[] myNumbers = new int[] { 1, 2, 3, 4, 5, 6 };
                //int[] myNumbers = { 1, 2, 3, 4, 5, 6 };


                //foreach (int number in myNumbers)
                //{
                //    Console.WriteLine(number);
                //}

                //Console.WriteLine(myNumbers[3]);
            }

            Car car = null;

            Console.WriteLine(car?.Wheels);

            Console.WriteLine("My fancy string".WordCount());
            Console.ReadKey();
        }

        static void CreateValue(out int someValue)
        {
            someValue = 100;
        }

        static void ChangeValue(Car someCar)
        {
            someCar.Wheels = 8;
        }

        static void ChangeValue(ref int number)
        {
            number = 50;
        }

        static void ChangeValue(string someString)
        {
            someString = "this is changed";
        }
    }
}
