﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstract_Revision
{
    interface IDrivable
    {
        float Speed { get; set; }

        float Accelerate(float speed);

        float Brake(float speed);
    }
}
