﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstract_Revision
{
    abstract class Vehicle : Machine
    {
        public float Speed { get; set; }

        public float Accelerate(float speed)
        {
            Speed = speed;
            return Speed;
        }

        abstract public float Deaccelerate(float speed);
    }
}
