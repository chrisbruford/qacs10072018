﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstract_Revision
{
    class Car : Vehicle, IDrivable, IFlyable
    {

        public float Brake(float speed)
        {
            Speed = speed;
            return Speed;
        }

        public override float Deaccelerate(float speed)
        {
            return Brake(speed);
        }
    }
}
