﻿using System;
using System.Collections.Generic;

namespace Abstract_Revision
{
    class Program
    {
        static void Main(string[] args)
        {

            //Boat vehicle = new Boat();
            //vehicle.Accelerate(100);
            //Console.WriteLine(vehicle.Sinking);
            //Console.WriteLine(vehicle.Speed);

            var drivables = new List<IDrivable>() { new RCCar(), new Car() }; ;
            //Object initialisation is a shortcut for the below:
            //drivables.Add(new RCCar());
            //drivables.Add(new Car());

            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
