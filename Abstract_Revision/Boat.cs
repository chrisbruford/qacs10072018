﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Abstract_Revision
{
    class Boat : Vehicle
    {
        public bool Sinking { get; set; }

        public override float Deaccelerate(float speed)
        {
            Speed = Speed;
            return Speed;
        }
    }

}
